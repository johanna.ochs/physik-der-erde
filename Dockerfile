# Specify parent image. Please select a fixed tag here.
ARG BASE_IMAGE=registry.git-ce.rwth-aachen.de/jupyter/singleuser/python:latest
FROM ${BASE_IMAGE}

# Install packages via requirements.txt
ADD environment.yml /tmp/environment.yml
USER root
RUN sed -i "s|name\: Physik der Erde|name\: base|g" /tmp/environment.yml

# Install mesa for 3D visualization with pyvista
RUN apt-get update && apt-get install -y libgl1-mesa-glx

USER $NB_USER

# All packages specified in environment.yml are installed in the base environment
RUN conda env update -f /tmp/environment.yml && \
    conda clean -a -f -y

ENV JUPYTER_ENABLE_LAB=yes


