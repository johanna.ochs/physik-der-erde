{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Altersbestimmung durch den Zerfall von radioaktiven Isotopen in Mineralen und Gesteinen\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Einfache Zerfallsgesetze\n",
    "Für die Altersbestimmung von Gesteinen gibt es einige oft genutzte Zerfallsreihen von Elementen, die häufig in bestimmten Mineralen vorkommen. Im linken Bild sind die Altersbereiche für diese Zerfallsreihen dargestellt. Die Anzahl der Elternisotope fällt über die Zeit ab ($P_0$ ist die Anfangsanzahl bei Entstehung). Je nachdem, wie lang die Halbwertszeit t$_{1/2}$ ist, können die Isotope für verschieden alte Gesteine benutzt werden. Es muss noch einen genügend großen Anteil des Elternisotops geben, aber auch schon einen gewissen Anteil des Tochterisotops für eine gute Auflösung der Methode. Je mehr das Elternisotop zerfällt, desto größer wird der Anteil des Tochterisotops (rechtes Bild).\n",
    "\n",
    "<img src=\"media/William_Lowrie_fig_8_4.jpg\" alt=\"Drawing\" style=\"width: 400px;\" align=\"left\"/>\n",
    "<img src=\"media/William_Lowrie_fig_8_3.jpg\" alt=\"Drawing\" style=\"width: 400px;\" align=\"right\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Der einfache Zerfall eines Elternisotops mit der zeitabhängigen Anzahl P wird beschrieben durch das Zerfallsgesetz:\n",
    "\n",
    "(1)  $\\frac{dP}{dt} = -\\lambda P$, mit der Zerfallskonstante $\\lambda$ (a$^{-1}$), die für jedes Isotop unterschiedlich ist.\n",
    "\n",
    "Die Lösung dieser Gleichung beschreibt den exponentiellen Zerfall:\n",
    "\n",
    "(2)  $P(t) = P_0\\cdot e^{-\\lambda t}$, mit der Anfangsanzahl $P_0$ zum Zeitpunkt $t=0$.\n",
    "\n",
    "Entsprechend können wir für die Anzahl D des (meist stabilen, nicht-radioaktiven) Tochterisotops die folgende Formel herleiten:\n",
    "\n",
    "(3)  $\\frac{dD}{dt} = \\lambda P$, mit der Lösung:\n",
    "\n",
    "(4)  $D(t) = P_0-P(t)$\n",
    "\n",
    "Die Anfangskonzentration des Elternisotops ist nicht bekannt, wenn wir die Isotope in einem beliebigen Gestein heutzutage messen. Wir messen eine Restanzahl des Elternisotops und eine Anzahl des Tochterisotops. Wenn wir die unbekannte Anfangsanzahl P$_0$ aus Gleichung 4 eliminieren, bekommen wir:\n",
    "\n",
    "(5)  $D(t) = P(t)(e^{\\lambda t}-1)$\n",
    "\n",
    "Manchmal werden in der Praxis **Isotopenverhältnisse** gemessen statt absolute Anzahlen an Isotopen. Dann werden in den Gleichungen die Anzahlen P, P$_0$ und D durch Verhältnisse ersetzt."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\underline{Aufgabe \\,1:}$ \n",
    "\n",
    "Die Halbwertszeiten der Uran-Isotope $^{235}U$ und $^{238}U$ sind $t_{\\frac{1}{2}}^{235} = 704\\,Ma$ und $t_{\\frac{1}{2}}^{238} = 4,468\\,Ga$. Berechnen Sie damit die Zerfallskonstanten $\\lambda_{235}$ und $\\lambda_{238}$.\n",
    "\n",
    "$\\underline{Aufgabe \\,2:}$\n",
    "\n",
    "Mit der Radiokarbon-Methode ($^{14}$C) wurde ein Stück Holz aus der Grabstätte eines Pharaos datiert. Die Messung ergab Konzentrationen von $9,83\\cdot 10^{-15}\\frac{mol}{g}$ für $^{14}$C und $1,202\\cdot 10^{-2}\\frac{mol}{g}$ für das stabile Zerfallsprodukt $^{12}$C. Wir gehen davon aus, dass das ursprüngliche Verhältnis $(\\frac{^{14}C}{^{12}C})_0$ dem langfristig stabilen atmosphärischem Wert von $1,20\\cdot 10^{-12}$ entsprach. Die Zerfallskonstante von $^{14}$C ist $\\lambda = 1,21\\cdot 10^{-4}$ $a^{-1}$. Bestimmen Sie das Alter der Grabstätte. Berechnen Sie anschließend den prozentualen Anteil des noch vorhandenen $^{14}$C-Isotops sowie die ursprüngliche Konzentration von $^{14}$C im Holz."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Die Isochronen-Methode am Beispiel der Rubidium-Strontium Zerfallsreihe\n",
    "In vielen Fällen können wir nicht davon ausgehen, dass das Tochter-Isotop ausschließlich durch den Zerfall des Elternisotops in einem geschlossenen System entstanden ist. Daher müssen wir eine unbekannte Anfangsmenge des Tochterisotops D$_0$ berücksichtigen:\n",
    "\n",
    "(6)  $D = D_0+P(e^{\\lambda t}-1)$\n",
    "\n",
    "Es gibt jedoch eine Methode, mit der wir D$_0$ nicht kennen müssen. Wenn es ein weiteres, **stabiles Isotop des Tochterelements** im Gestein gibt, können wir damit die Anzahl des Eltern- und Tochterisotops, deren Zerfall wir untersuchen, normalisieren. Wenn wir mindestens zwei Proben mit verschiedenen Verhältnissen von Eltern- zu Tochterisotop gewinnen können (z.B. zwei Minerale aus dem gleichen Gestein), können wir die relative Anreicherung des Tochterisotops und damit das Alter des Gesteins durch die Isochronen-Methode ermitteln. Ein prominentes Beispiel für die Isochronen-Methode ist die Rubidium-Strontium Zerfallsreihe, die aufgrund der langen Halbwertszeit (Zerfallskonstante $\\lambda_{Rb}=1,42\\cdot 10^{-11}\\,a^{-1}$) für sehr alte Gesteine verwendet werden kann.\n",
    "\n",
    "Radioaktives Rubidium ($^{87}$Rb) zerfällt in das ebenfalls radioaktive Strontium ($^{87}$Sr). Das stabile Strontium-Isotop $^{86}$Sr ist in etwa in der gleichen Häufigkeit vorhanden wie $^{87}$Sr. Wir nutzen es für die Normalisierung und betrachten **Verhältnisse statt Anzahlen** (vergleiche mit Gleichung 6):\n",
    "\n",
    "(7)  $(\\frac{^{87}Sr}{^{86}Sr})=(\\frac{^{87}Sr}{^{86}Sr})_0+(\\frac{^{87}Rb}{^{86}Sr})\\cdot(e^{\\lambda t}-1)$\n",
    "\n",
    "Um die Isochronen-Methode zu nutzen, tragen wir in einem Graphen das normalisierte Elternisotop ($\\frac{^{87}Rb}{^{86}Sr}$) auf der x-Achse auf und das normalisierte Tochterisotop ($\\frac{^{87}Sr}{^{86}Sr}$) auf der y-Achse. Gleichung 7 hat die **Form einer Geraden**, also einer Funktion der Form $y=y_0+m\\cdot x$. Wenn wir also die eingetragenen Wertepaare mit einer Geraden fitten, entspricht die unabhängige Variable x dem **normalisierten Elternisotop** und die abhängige Variable y dem **normalisierten Tochterisotop**. Diese Gerade wird **Isochrone** genannt und ihre Steigung m $(=e^{\\lambda t}-1)$  erlaubt uns die Berechnung des Gesteinsalters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"media/William_Lowrie_fig_8_6_alt.png\" alt=\"Drawing\" style=\"width: 400px;\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\underline{Aufgabe \\,3:}$\n",
    "\n",
    "Die in der Abbildung aufgetragenen Rubidium-Strontium Verhältnisse wurden in einem präkambrischen Gneis aus Labrador (Uivak Gneis) gemessen. Bestimmen Sie aus dem Plot das ungefähre Alter der Gesteinsproben in Millionen Jahren mit drei signifikanten Stellen. \n",
    "\n",
    "Suchen Sie sich ein Messwertepaar raus und berechnen Sie damit die ungefähre normierte Menge des Tochterisotops $(\\frac{^{87}Sr}{^{86}Sr})_0$ zur Zeit der Bildung des Gneis. Wie könnte sie alternativ bestimmt werden?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
